package net.voronoff.yameeting;

import android.app.Application;
import android.util.Log;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import net.voronoff.yameeting.injection.component.ApplicationComponent;
import net.voronoff.yameeting.injection.component.DaggerApplicationComponent;
import net.voronoff.yameeting.injection.module.ApplicationModule;

public class YAMeetingApplication extends Application {
    private static final String TAG = "YAMeetingApplication";

    private static ApplicationComponent applicationComponent;

    private VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                Log.w(TAG, "VKAccessToken is invalid!");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);
    }

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
