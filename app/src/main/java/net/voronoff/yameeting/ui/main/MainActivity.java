package net.voronoff.yameeting.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.ui.YAMDialog;
import net.voronoff.yameeting.ui.auth.AuthActivity;
import net.voronoff.yameeting.ui.base.BaseActivity;
import net.voronoff.yameeting.ui.event.EventActivity;
import net.voronoff.yameeting.ui.main.exceptions.EventIsNullException;
import net.voronoff.yameeting.ui.main.fragments.EventDetailFragment;
import net.voronoff.yameeting.ui.main.fragments.EventListFragment;
import net.voronoff.yameeting.ui.picasso.CircleTransform;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        MainMvpView,
        EventListFragment.OnFragmentInteractionListener,
        EventDetailFragment.OnFragmentInteractionListener {
    @SuppressWarnings("unused")
    private static final String TAG = "MainActivity";

    private static final String FRAGMENT_EVENTS_LIST  = "FRAGMENT_EVENTS_LIST";
    private static final String FRAGMENT_EVENT_DETAIL = "FRAGMENT_EVENT_DETAIL";

    private static final String BACK_STACK_EVENT_DETAIL = "BACK_STACK_EVENT_DETAIL";

    private static final int REQUEST_AUTH = 1;

    @Inject
    MainPresenter presenter;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    @Bind(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        presenter.attachView(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        popBackStackEventDetail();

        FragmentManager fm = getSupportFragmentManager();

        EventListFragment eventListFragment;
        eventListFragment = (EventListFragment) fm.findFragmentByTag(FRAGMENT_EVENTS_LIST);
        if (eventListFragment == null) {
            eventListFragment = EventListFragment.newInstance();
        }

        fm.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.main_container_placeholder_1, eventListFragment, FRAGMENT_EVENTS_LIST)
                .commit();

        setAvatar(null);
        setUserName(null);

        presenter.onMainActivityCreated(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_AUTH && resultCode == RESULT_OK) {
            presenter.refresh();
            presenter.onAuthorized(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            presenter.refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            presenter.onBackPressed(this);
            super.onBackPressed();
        }
    }

    @OnClick(R.id.fab)
    void onClickFab() {
        Intent intent = new Intent(getApplicationContext(), EventActivity.class);
        startActivity(intent);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_auth) {
            showAuthForm();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showEventDetail(Event event) throws EventIsNullException {
        if (event == null)
            throw new EventIsNullException();

        @IdRes int containerViewId;
        if (isDualPane())
            containerViewId = R.id.main_container_placeholder_2;
        else
            containerViewId = R.id.main_container_placeholder_1;

        EventDetailFragment eventDetailFragment = EventDetailFragment.newInstance(event);
        eventDetailFragment.setOnInteractionListener(presenter);

        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
//                        android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(containerViewId, eventDetailFragment, FRAGMENT_EVENT_DETAIL);

        if (!isDualPane())
            transaction.addToBackStack(BACK_STACK_EVENT_DETAIL);

        transaction.commit();
    }

    @Override
    public void showError(@NotNull String message) {
        YAMDialog.showMessage(getSupportFragmentManager(), message);
    }

    @Override
    public void showInfo(@NotNull String message) {
        View view = findViewById(R.id.coordinator);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showAuthForm() {
        Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
        startActivityForResult(intent, REQUEST_AUTH);
    }

    @Override
    public boolean isDualPane() {
        return findViewById(R.id.main_container_placeholder_2) != null;
    }

    @Override
    public void setAvatar(String uri) {
        ImageView avatar = (ImageView) getNavDrawerHeader().findViewById(R.id.iv_nav_header_avatar);
        RequestCreator requestCreator;
        if (uri == null) {
            requestCreator = Picasso.with(this).load(R.drawable.nav_header_default_avatar);
        } else {
            requestCreator = Picasso.with(this).load(uri);
        }
        requestCreator.resize(100, 100)
                .centerInside()
                .transform(new CircleTransform())
                .into(avatar);
    }

    @Override
    public void setUserName(String name) {
        TextView tvName = (TextView) getNavDrawerHeader().findViewById(R.id.tv_hav_header_name);
        tvName.setText(name == null ? getString(R.string.nav_header_default_name) : name);
    }

    @Nullable
    @Override
    public EventListSubView getEventListSubView() {
        return (EventListSubView) getSupportFragmentManager()
                .findFragmentByTag(FRAGMENT_EVENTS_LIST);
    }

    @Nullable
    @Override
    public EventDetailSubView getEventDetailSubView() {
        return (EventDetailSubView) getSupportFragmentManager()
                .findFragmentByTag(FRAGMENT_EVENT_DETAIL);
    }

    @Override
    public void onListenerRequest(EventListFragment eventListFragment) {
        eventListFragment.setOnInteractionListener(presenter);
    }

    @Override
    public void onListenerRequest(EventDetailFragment eventDetailFragment) {
        eventDetailFragment.setOnInteractionListener(presenter);
    }

    private void popBackStackEventDetail() {
        getSupportFragmentManager()
                .popBackStack(BACK_STACK_EVENT_DETAIL, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @NonNull
    private View getNavDrawerHeader() {
        return navigationView.getHeaderView(0);
    }
}
