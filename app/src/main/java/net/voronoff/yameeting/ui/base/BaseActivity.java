package net.voronoff.yameeting.ui.base;

import android.support.v7.app.AppCompatActivity;

import net.voronoff.yameeting.YAMeetingApplication;
import net.voronoff.yameeting.injection.component.ActivityComponent;
import net.voronoff.yameeting.injection.module.ActivityModule;

public class BaseActivity extends AppCompatActivity implements MvpView {
    private ActivityComponent activityComponent;

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = YAMeetingApplication.getApplicationComponent()
                    .plusActivityComponent(new ActivityModule(this));
        }
        return activityComponent;
    }
}
