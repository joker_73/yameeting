package net.voronoff.yameeting.ui.main.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.YAMeetingApplication;
import net.voronoff.yameeting.data.TokenManager;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.injection.ActivityContext;
import net.voronoff.yameeting.injection.ApplicationContext;
import net.voronoff.yameeting.ui.main.EventListSubView;
import net.voronoff.yameeting.ui.main.exceptions.WrongPositionException;
import net.voronoff.yameeting.utils.ApplicationUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> implements
        Parcelable,
        EventListSubView.DataKeeper {
    @SuppressWarnings("unused")
    private static final String TAG = "EventsAdapter";

    public static final int UNKNOWN_POSITION = -1;

    @Inject
    @ApplicationContext
    Context context;

    @Inject
    ApplicationUtils utils;

    @Inject
    TokenManager tokenManager;

    private final ArrayList<Event> events;
    private OnAdapterInteractionListener listener;
    private int selectedItemPosition = UNKNOWN_POSITION;
    private boolean selectable = false;

    private Drawable subscribed;
    private Drawable notSubscribed;

    public EventsAdapter() {
        init();
        events = new ArrayList<>();
    }

    protected EventsAdapter(Parcel in) {
        init();
        events = in.createTypedArrayList(Event.CREATOR);
        selectedItemPosition = in.readInt();
        selectable = in.readByte() != 0;
    }

    private void init() {
        YAMeetingApplication.getApplicationComponent().inject(this);
        subscribed = context.getResources().getDrawable(R.drawable.ic_event_item_subscribed);
        notSubscribed = context.getResources().getDrawable(R.drawable.ic_event_item_not_subscribed);
    }

    public static final Creator<EventsAdapter> CREATOR = new Creator<EventsAdapter>() {
        @Override
        public EventsAdapter createFromParcel(Parcel in) {
            return new EventsAdapter(in);
        }

        @Override
        public EventsAdapter[] newArray(int size) {
            return new EventsAdapter[size];
        }
    };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.parent.setSelected(selectable && position == selectedItemPosition);

        Event event = events.get(position);
        holder.name.setText(event.getName());
        holder.address.setText(event.getAddress());
        holder.where.setText(utils.toString(event.getWhere()));
        holder.num.setText(String.valueOf(event.getSubscribersNumber()));
        if (tokenManager.isAuthorized()) {
            holder.subscribed.setVisibility(View.VISIBLE);
            holder.subscribed.setImageDrawable(event.isSubscribed() ? subscribed : notSubscribed);
            holder.my.setVisibility(event.isMy() ? View.VISIBLE : View.INVISIBLE);
        } else {
            holder.subscribed.setVisibility(View.INVISIBLE);
            holder.my.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(events);
        dest.writeInt(selectedItemPosition);
        dest.writeByte((byte) (selectable ? 1 : 0));
    }

    @Override
    public void addEvents(List<Event> events) {
        for (Event event : events)
            addEvent(event);
    }

    @Override
    public void addEvent(Event event) {
        events.add(event);
        notifyItemInserted(events.size() - 1);
    }

    @Override
    public void removeEvent(int position) {
        events.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void clearEvents() {
        int size = events.size();
        events.clear();
        notifyItemRangeRemoved(0, size);
    }

    @Override
    public void updateCurrent(Event event) {
        events.set(selectedItemPosition, event);
        notifyItemChanged(selectedItemPosition);
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public int getSelectedItemPosition() {
        return selectedItemPosition;
    }

    public void setSelectedItemPosition(int selectedItemPosition) {
        this.selectedItemPosition = selectedItemPosition;
    }

    public void selectEvent(boolean click) {
        try {
            selectEvent(click, selectedItemPosition);
        } catch (WrongPositionException ignored) { }
    }

    public void selectEvent(boolean click, int position) throws WrongPositionException {
        if (position == UNKNOWN_POSITION)
            position = 0;

        if (position < 0 || position >= getItemCount())
            throw new WrongPositionException();

        /* (  ._.)
         * http://stackoverflow.com/questions/34191427/recyclerview-item-not-showing-ripples-touch-feedback-when-the-item-has-a-checkbo
         *
         * In your onClick method (assuming in your callback), make sure you do not call
         * notifyDataSetChanged() after notifyItemChanged(position).
         *
         * notifyDataSetChanged() will conflict with those default ripple effects.
         */
        notifyItemChanged(selectedItemPosition);
        selectedItemPosition = position;
        notifyItemChanged(selectedItemPosition);

        if (listener != null) {
            Event event = events.get(selectedItemPosition);
            listener.onEventSelected(click, event, selectedItemPosition);
        }
    }

    public void setOnAdapterInteractionListener(OnAdapterInteractionListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View parent;

        @Bind(R.id.event_item_name)
        public TextView name;

        @Bind(R.id.event_item_address)
        public TextView address;

        @Bind(R.id.event_item_where)
        public TextView where;

        @Bind(R.id.event_item_num)
        public TextView num;

        @Bind(R.id.event_itme_subscribed)
        public ImageView subscribed;

        @Bind(R.id.event_item_my)
        public ImageView my;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

            parent = itemView;
        }

        @Override
        public void onClick(View v) {
            try {
                selectEvent(true, getAdapterPosition());
            } catch (WrongPositionException ignored) { }
        }
    }

    public interface OnAdapterInteractionListener {
        void onEventSelected(boolean click, Event event, int position);
    }
}
