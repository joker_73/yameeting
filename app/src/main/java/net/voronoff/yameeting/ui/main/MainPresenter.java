package net.voronoff.yameeting.ui.main;

import android.content.Context;
import android.util.Log;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.DataManager;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.injection.ActivityContext;
import net.voronoff.yameeting.ui.base.BasePresenter;
import net.voronoff.yameeting.ui.main.exceptions.EventIsNullException;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class MainPresenter extends BasePresenter<MainMvpView> implements
        EventListSubView.OnInteractionListener,
        EventDetailSubView.OnInteractionListener {
    @SuppressWarnings("unused")
    private static final String TAG = "MainPresenter";

    @ActivityContext
    private final Context context;
    private final DataManager dataManager;

    private CompositeSubscription subscription = new CompositeSubscription();

    private boolean getEventsInProgress = false;

    public MainPresenter(@ActivityContext Context context, DataManager dataManager) {
        this.context = context;
        this.dataManager = dataManager;
    }

    @Override
    public void detachView() {
        subscription.unsubscribe();
        super.detachView();
    }

    @Override
    public void onFirstLoad(EventListSubView eventListSubView) {
        getEvents(true);
    }

    @Override
    public void onReady(EventListSubView eventListSubView) {
        eventListSubView.setSelectable(getMvpView().isDualPane());
    }

    @Override
    public void onEventSelected(Event event, int position) {
        MainMvpView mainMvpView = getMvpView();
        EventListSubView eventListSubView = mainMvpView.getEventListSubView();
        if (mainMvpView.isDualPane() || (eventListSubView != null && eventListSubView.isClicked())){
            try {
                mainMvpView.showEventDetail(event);
            } catch (EventIsNullException ignored) { }
        }
    }

    @Override
    public void onClickSubscribe(final EventDetailSubView eventDetailSubView, final Event event) {
        dataManager.setSubscribe(event.getId(), !event.isSubscribed())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newEvent -> {
                    eventDetailSubView.setEvent(newEvent);
                    EventListSubView eventListSubView = getMvpView().getEventListSubView();
                    if (eventListSubView != null) {
                        eventListSubView.getDataKeeper().updateCurrent(newEvent);
                    }
                }, Throwable::printStackTrace);
    }

    public void onMainActivityCreated(MainMvpView mainMvpView) {
        getMe(mainMvpView);
    }

    public void onBackPressed(MainMvpView mainMvpView) {
        EventListSubView eventListSubView = mainMvpView.getEventListSubView();
        if (eventListSubView != null) {
            eventListSubView.setClicked(false);
        }
    }

    public void onAuthorized(final MainMvpView mainMvpView) {
        getMe(mainMvpView);
    }

    public void refresh() {
        getEvents(getMvpView().isDualPane());
    }

    private void getMe(final MainMvpView mainMvpView) {
        subscription.add(
                dataManager.getMe()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(me -> {
                            mainMvpView.setUserName(me.toString());
                            mainMvpView.setAvatar(dataManager.getAvatarUrl(me));
                        }, throwable -> Log.w(TAG, "Failed getting \"Me\""))
        );
    }

    private void getEvents(boolean select) {
        if (getEventsInProgress) {
            getMvpView().showInfo(context.getString(R.string.refresh_in_progress));
            return;
        }

        getEventsInProgress = true;

        Log.v(TAG, "Load events");

        subscription.add(
                dataManager.getEvents()
                        .cache()
                        .take(1)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new EventGetterObserver(select))
        );
    }

    private class EventGetterObserver implements Observer<List<Event>> {
        private boolean select;

        private EventGetterObserver(boolean select) {
            this.select = select;
        }

        @Override
        public void onCompleted() {
            getEventsInProgress = false;
        }

        @Override
        public void onError(Throwable e) {
            if (e.getMessage().equals("HTTP 401 Unauthorized")) {
                getMvpView().showAuthForm();
            } else {
                getMvpView().showError(context.getString(R.string.data_not_loaded));
                e.printStackTrace();
            }
            getEventsInProgress = false;
        }

        @Override
        public void onNext(List<Event> events) {
            EventListSubView eventListSubView = getMvpView().getEventListSubView();
            if (eventListSubView != null) {
                eventListSubView.getDataKeeper().clearEvents();
                eventListSubView.getDataKeeper().addEvents(events);
                if (select) {
                    eventListSubView.select();
                }
            }
        }
    }
}
