package net.voronoff.yameeting.ui.main;

import android.support.annotation.Nullable;

import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.ui.base.MvpView;
import net.voronoff.yameeting.ui.main.exceptions.EventIsNullException;

import org.jetbrains.annotations.NotNull;

public interface MainMvpView extends MvpView {
    void showEventDetail(Event event) throws EventIsNullException;
    void showError(@NotNull String message);
    void showInfo(@NotNull String message);
    void showAuthForm();
    boolean isDualPane();

    void setAvatar(String uri);
    void setUserName(String name);

    @Nullable
    EventListSubView getEventListSubView();
    @Nullable
    EventDetailSubView getEventDetailSubView();
}
