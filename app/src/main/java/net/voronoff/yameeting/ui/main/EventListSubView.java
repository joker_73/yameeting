package net.voronoff.yameeting.ui.main;

import net.voronoff.yameeting.data.model.Event;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface EventListSubView {
    void setSelectable(boolean selectable);
    void select();
    boolean isClicked();
    void setClicked(boolean clicked);
    @NotNull
    DataKeeper getDataKeeper();

    interface DataKeeper {
        void addEvents(List<Event> events);
        void addEvent(Event event);
        void removeEvent(int position);
        void clearEvents();
        void updateCurrent(Event event);
    }

    interface OnInteractionListener {
        void onFirstLoad(EventListSubView eventListSubView);
        void onReady(EventListSubView eventListSubView);
        void onEventSelected(Event event, int position);
    }
}
