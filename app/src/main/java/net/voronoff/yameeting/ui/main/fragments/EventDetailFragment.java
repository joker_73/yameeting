package net.voronoff.yameeting.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.injection.ActivityContext;
import net.voronoff.yameeting.ui.base.BaseFragment;
import net.voronoff.yameeting.ui.main.EventDetailSubView;
import net.voronoff.yameeting.utils.ApplicationUtils;

import javax.inject.Inject;

import at.markushi.ui.CircleButton;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EventDetailFragment extends BaseFragment implements
        EventDetailSubView {
    private static final String ARG_EVENT = "ARG_EVENT";

    @Inject
    @ActivityContext
    Context context;

    @Inject
    ApplicationUtils utils;

    @Bind(R.id.iv_details_header)
    ImageView header;

    @Bind(R.id.tv_details_name)
    TextView tvName;

    @Bind(R.id.tv_details_address)
    TextView tvAddress;

    @Bind(R.id.tv_details_where)
    TextView tvWhere;

    @Bind(R.id.tv_details_description)
    TextView tvDescription;

    @Bind(R.id.fragment_event_detail_subscribe)
    CircleButton subscribeField;

    private Event event;

    private OnFragmentInteractionListener parent;
    private OnInteractionListener listener;

    public EventDetailFragment() {
        // Required empty public constructor
    }

    public static EventDetailFragment newInstance(Event event) {
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            parent = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getFragmentComponent().inject(this);

        super.onCreate(savedInstanceState);

        parent.onListenerRequest(this);

        if (getArguments() != null) {
            event = getArguments().getParcelable(ARG_EVENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        getFragmentComponent().inject(this);

        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);
        ButterKnife.bind(this, view);

        update();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        listener = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @OnClick(R.id.fragment_event_detail_subscribe)
    void onClickSubscribe() {
        if (listener != null) {
            listener.onClickSubscribe(this, event);
        }
    }

    @Override
    public void setEvent(Event event) {
        this.event = event;
        update();
    }

    private void update() {
        if (event.isSubscribed()) {
            subscribeField.setImageDrawable(getResources().getDrawable(R.drawable.ic_fragment_event_detail_subscribe_no));
        } else {
            subscribeField.setImageDrawable(getResources().getDrawable(R.drawable.ic_fragment_event_detail_subscribe_yes));
        }
        Picasso.with(context)
                .load(R.drawable.header)
                .fit()
                .centerCrop()
                .into(header);
        tvName.setText(event.getName());
        tvAddress.setText(event.getAddress());
        tvWhere.setText(utils.toString(event.getWhere()));
        tvDescription.setText(event.getDesc());
    }

    public void setOnInteractionListener(OnInteractionListener listener) {
        this.listener = listener;
    }

    public interface OnFragmentInteractionListener {
        void onListenerRequest(EventDetailFragment eventDetailFragment);
    }
}
