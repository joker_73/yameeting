package net.voronoff.yameeting.ui.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.TokenManager;
import net.voronoff.yameeting.data.model.Token;
import net.voronoff.yameeting.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class AuthActivity extends BaseActivity {
    private static final String TAG = "AuthActivity";

    @Bind(R.id.actv_auth_username)
    AutoCompleteTextView username;

    @Bind(R.id.et_auth_password)
    EditText password;

    @Bind(R.id.login_progress)
    View progressView;

    @Bind(R.id.login_form)
    View loginFormView;

    @Bind(R.id.b_auth_vk_logout)
    Button vkLogout;

    @Inject
    TokenManager tokenManager;

    private CompositeSubscription subscription = new CompositeSubscription();

    private Observer<Token> tokenObservable = new Observer<Token>() {
        @Override
        public void onCompleted() {
            Log.d(TAG, "onCompleted");
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError");
            e.printStackTrace();
            VKSdk.logout();
        }

        @Override
        public void onNext(Token token) {
            Log.d(TAG, "onNext: " + token);
        }
    };

    private VKCallback<VKAccessToken> vkCallback = new VKCallback<VKAccessToken>() {
        @Override
        public void onResult(VKAccessToken res) {
            convertToken(res);
        }

        @Override
        public void onError(VKError error) {
            Log.w(TAG, "Vk auth error: " + error.errorMessage);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        setupActionBar();

        if (!VKSdk.isLoggedIn()) {
            vkLogout.setEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        subscription.unsubscribe();
        super.onDestroy();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (VKSdk.onActivityResult(requestCode, resultCode, data, vkCallback))
            return;

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.b_auth_sign_in)
    void onClickSignIn(View view) {
        subscription.add(
                tokenManager.getToken(username.getText().toString(), password.getText().toString())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tokenObservable)
        );
    }

    @OnClick(R.id.b_auth_vk_sign_in)
    void onClickVkSignIn(View view) {
        if (!VKSdk.isLoggedIn()) {
            Log.d(TAG, "Not loggedIn");
            String[] sMyScope = new String[]{
                    VKScope.EMAIL,
            };
            VKSdk.login(this, sMyScope);
        } else {
            Log.d(TAG, "Already loggedIn");
            convertToken(VKAccessToken.currentToken());
        }
    }

    @OnClick(R.id.b_auth_vk_logout)
    void onClickVkLogout() {
        VKSdk.logout();
    }

    private void convertToken(VKAccessToken vkAccessToken) {
        subscription.add(
                tokenManager.covertToken(vkAccessToken)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tokenObservable)
        );
    }

    @OnClick(R.id.b_auth_drop_token)
    void onClickDropToken() {
        tokenManager.revokeToken()
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "Revoke token: onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Revoke token: onError");
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        Log.d(TAG, "Revoke token: onNext");
                    }
                });
    }
}

