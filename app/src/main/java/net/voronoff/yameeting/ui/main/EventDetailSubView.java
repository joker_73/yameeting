package net.voronoff.yameeting.ui.main;

import net.voronoff.yameeting.data.model.Event;

public interface EventDetailSubView {
    void setEvent(Event event);

    interface OnInteractionListener {
        void onClickSubscribe(EventDetailSubView eventDetailSubView, Event event);
    }
}
