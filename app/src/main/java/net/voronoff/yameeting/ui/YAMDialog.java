package net.voronoff.yameeting.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import org.jetbrains.annotations.NotNull;

public class YAMDialog extends DialogFragment {
    private static final String KEY_MESSAGE = "KEY_MESSAGE";

    private String message;

    public static void showMessage(@NotNull FragmentManager fragmentManager, @NotNull String message) {
        newInstance(message).show(fragmentManager, null);
    }

    public static YAMDialog newInstance(String message) {
        YAMDialog yamDialog = new YAMDialog();
        Bundle args = new Bundle();
        args.putString(KEY_MESSAGE, message);
        yamDialog.setArguments(args);
        return yamDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle args = getArguments();
            if (args == null)
                throw new IllegalArgumentException("Bundle args required");
            message = args.getString(KEY_MESSAGE);
        } else {
            message = savedInstanceState.getString(KEY_MESSAGE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_MESSAGE, message);
    }


    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message).setNeutralButton(android.R.string.ok, (dialog, which) -> { });
        return builder.create();
    }
}
