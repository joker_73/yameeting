package net.voronoff.yameeting.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import net.voronoff.yameeting.injection.component.FragmentComponent;

public class BaseFragment extends Fragment {
    private FragmentComponent fragmentComponent;
    private BaseActivity baseActivity;

    protected FragmentComponent getFragmentComponent() {
        if (fragmentComponent == null) {
            fragmentComponent = baseActivity.getActivityComponent()
                    .plusFragmentComponent();
        }
        return fragmentComponent;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity) {
            baseActivity = (BaseActivity) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement BaseActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        baseActivity = null;
    }
}
