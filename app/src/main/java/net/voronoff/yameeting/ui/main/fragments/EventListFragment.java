package net.voronoff.yameeting.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.ui.base.BaseFragment;
import net.voronoff.yameeting.ui.main.EventListSubView;

import org.jetbrains.annotations.NotNull;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventListFragment extends BaseFragment implements
        EventListSubView,
        EventsAdapter.OnAdapterInteractionListener {
    @SuppressWarnings("unused")
    private static final String TAG = "EventListFragment";

    private static final String SAVE_CLICK    = "SAVE_CLICK";
    private static final String SAVE_ADAPTER  = "SAVE_ADAPTER";

    @Bind(R.id.rv_list)
    RecyclerView recyclerView;

    private EventsAdapter eventsAdapter;
    private LinearLayoutManager layoutManager;
    private OnFragmentInteractionListener parent;
    private OnInteractionListener listener;
    private boolean clicked = false;

    public EventListFragment() {
        // Required empty public constructor
    }

    public static EventListFragment newInstance() {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            parent = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getFragmentComponent().inject(this);

        super.onCreate(savedInstanceState);

        parent.onListenerRequest(this);

        if (savedInstanceState != null) {
            eventsAdapter = savedInstanceState.getParcelable(SAVE_ADAPTER);
        }
        if (eventsAdapter == null) {
            eventsAdapter = new EventsAdapter();
        }
        eventsAdapter.setOnAdapterInteractionListener(this);
        layoutManager = new LinearLayoutManager(getContext());

        if (savedInstanceState == null && listener != null) {
            listener.onFirstLoad(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        ButterKnife.bind(this, view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(eventsAdapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new ItemDecoration(getContext()));

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            clicked = savedInstanceState.getBoolean(SAVE_CLICK);
        }

        if (listener != null) {
            listener.onReady(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        select();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVE_CLICK, clicked);
        outState.putParcelable(SAVE_ADAPTER, eventsAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerView.setLayoutManager(null);
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        listener = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public void onEventSelected(boolean click, Event event, int position) {
        this.clicked = click;
        if (listener != null) {
            listener.onEventSelected(event, position);
        }
    }

    @Override
    public void setSelectable(boolean selectable) {
        eventsAdapter.setSelectable(selectable);
    }

    @Override
    public void select() {
        eventsAdapter.selectEvent(clicked);

        if (clicked) {
            layoutManager.scrollToPositionWithOffset(eventsAdapter.getSelectedItemPosition(), 50);
        }
    }

    @Override
    public boolean isClicked() {
        return clicked;
    }

    @Override
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    @NotNull
    @Override
    public DataKeeper getDataKeeper() {
        assert eventsAdapter != null;
        return eventsAdapter;
    }

    public void setOnInteractionListener(OnInteractionListener listener) {
        this.listener = listener;
    }

    public interface OnFragmentInteractionListener {
        void onListenerRequest(EventListFragment eventListFragment);
    }
}
