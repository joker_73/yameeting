package net.voronoff.yameeting.ui.event;

import java.util.Calendar;
import java.util.Date;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class RxCalendar {
    private Calendar calendar;
    private BehaviorSubject<Date> subject;

    public RxCalendar() {
        calendar = Calendar.getInstance();
        subject = BehaviorSubject.create(calendar.getTime());
    }

    public Observable<Date> getObservable() {
        return subject;
    }

    public void setDate(int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        subject.onNext(calendar.getTime());
    }

    public void setTime(int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        subject.onNext(calendar.getTime());
    }

    public void setDate(Date date) {
        calendar.setTime(date);
        subject.onNext(calendar.getTime());
    }

    public Date getDate() {
        return calendar.getTime();
    }

    public int getYear() {
        return calendar.get(Calendar.YEAR);
    }

    public int getMonth() {
        return calendar.get(Calendar.MONTH);
    }

    public int getDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public int getHour() {
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinute() {
        return calendar.get(Calendar.MINUTE);
    }
}
