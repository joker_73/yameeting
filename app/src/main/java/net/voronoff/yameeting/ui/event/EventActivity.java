package net.voronoff.yameeting.ui.event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;

import net.voronoff.yameeting.R;
import net.voronoff.yameeting.data.DataManager;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.ui.YAMDialog;
import net.voronoff.yameeting.ui.base.BaseActivity;
import net.voronoff.yameeting.utils.ApplicationUtils;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;

import at.markushi.ui.CircleButton;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class EventActivity extends BaseActivity {
    private static final String TAG = "EventActivity";

    private static final String SAVE_CALENDAR = "SAVE_CALENDAR";

    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    DataManager dataManager;

    @Inject
    ApplicationUtils applicationUtils;

    @Bind(R.id.activity_event_name)
    EditText nameField;
    private Checker<CharSequence> nameChecker = new Checker<CharSequence>() {
        @Override
        boolean check(CharSequence name) {
            if (name.length() == 0) {
                return false;
            }
            if (name.length() > 255) {
                nameField.setError(getString(R.string.activity_event_err_too_many_symbols_name));
                return false;
            }
            nameField.setError(null);
            return true;
        }
    };

    @Bind(R.id.activity_event_address)
    EditText addressField;
    private Checker<CharSequence> addressChecker = new Checker<CharSequence>() {
        @Override
        boolean check(CharSequence address) {
            if (address.length() == 0) {
                return false;
            }
            if (address.length() > 512) {
                addressField.setError(getString(R.string.activity_event_err_too_many_symbols_address));
                return false;
            }
            addressField.setError(null);
            return true;
        }
    };

    @Bind(R.id.activity_event_where)
    TextView whereField;
    private RxCalendar rxCalendar = new RxCalendar();
    private Checker<Date> whereChecker = new Checker<Date>() {
        @Override
        boolean check(Date value) {
            whereField.setText(applicationUtils.toString(value));
            if (!value.after(new Date())) {
                whereField.setError(getString(R.string.activity_event_err_where));
                return false;
            }
            whereField.setError(null);
            return true;
        }
    };

    @Bind(R.id.activity_event_desc)
    EditText descField;
    private Checker<CharSequence> descChecker = new Checker<CharSequence>() {
        @Override
        boolean check(CharSequence desc) {
            if (desc.length() == 0) {
                return false;
            }
            if (desc.length() > 512) {
                descField.setError(getString(R.string.activity_event_err_too_many_symbols_description));
                return false;
            }
            descField.setError(null);
            return true;
        }
    };

    @Bind(R.id.activity_event_add)
    CircleButton addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);

        subscription.add(
                Observable.combineLatest(
                        RxTextView.textChanges(nameField).compose(nameChecker),
                        RxTextView.textChanges(addressField).compose(addressChecker),
                        RxTextView.textChanges(descField).compose(descChecker),
                        rxCalendar.getObservable().compose(whereChecker),
                        (name, address, desc, where) -> name && address && desc && where
                ).subscribe(ok -> addButton.setVisibility(ok ? View.VISIBLE : View.GONE))
        );
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        rxCalendar.setDate(new Date(savedInstanceState.getLong(SAVE_CALENDAR)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(SAVE_CALENDAR, rxCalendar.getDate().getTime());
    }

    @Override
    protected void onDestroy() {
        subscription.unsubscribe();
        super.onDestroy();
    }

    @OnClick(R.id.activity_event_where)
    void onClickWhere() {
        whereField.requestFocus();
    }

    @OnFocusChange(R.id.activity_event_where)
    void onFocusChange() {
        whereField.setText(applicationUtils.toString(rxCalendar.getDate()));
    }

    @OnClick(R.id.activity_event_select_date)
    void onClickSelectDate() {
        new DatePickerDialog(
                this,
                (view, year, month, day) -> rxCalendar.setDate(year, month, day),
                rxCalendar.getYear(),
                rxCalendar.getMonth(),
                rxCalendar.getDay()
        ).show();
    }

    @OnClick(R.id.activity_event_select_time)
    void onClickSelectTime() {
        new TimePickerDialog(
                this,
                (view, hourOfDay, minute) -> rxCalendar.setTime(hourOfDay, minute),
                rxCalendar.getHour(),
                rxCalendar.getMinute(),
                true
        ).show();
    }

    @OnClick(R.id.activity_event_add)
    void onClickAdd() {
        String name = nameField.getText().toString();
        String address = addressField.getText().toString();
        Date where = new Date();
        String desc = descField.getText().toString();

        Event event = new Event(name, address, where, desc);

        subscription.add(
                dataManager.addEvent(event)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .cache()
                        .subscribe(event1 -> finish(), throwable -> {
                            StringBuilder sb = new StringBuilder();
                            sb.append(getString(R.string.activity_event_err_add_new_meeting))
                                    .append("\n")
                                    .append(throwable.getMessage());
                            if (throwable instanceof HttpException) {
                                try {
                                    String error = ((HttpException) throwable).response()
                                            .errorBody()
                                            .string();
                                    Log.w(TAG, error);
                                    sb.append("\n").append(error);
                                } catch (IOException ignore) { }
                            }
                            YAMDialog.showMessage(getSupportFragmentManager(), sb.toString());
                        })
        );
    }

    private abstract class Checker<T> implements Observable.Transformer<T, Boolean> {
        @Override
        public Observable<Boolean> call(Observable<T> observable) {
            return observable
                    .map(this::check)
                    .distinctUntilChanged();
        }

        abstract boolean check(T value);
    }
}
