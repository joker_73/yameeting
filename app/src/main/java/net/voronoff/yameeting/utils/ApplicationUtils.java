package net.voronoff.yameeting.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;

import net.voronoff.yameeting.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ApplicationUtils {
    private Context context;

    public ApplicationUtils(Context context) {
        this.context = context;
    }

    public String toString(Date date) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat(context.getString(R.string.date_time_format));
        return sdf.format(date);
    }

    public boolean isLandscapeOrientation() {
        int currentOrientation = context.getResources().getConfiguration().orientation;
        return currentOrientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
}
