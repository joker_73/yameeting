package net.voronoff.yameeting.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;

public class BitmapUtils {
    private static final String TAG = "BitmapUtils";

    private Context context;

    public BitmapUtils(Context context) {
        this.context = context;
    }

    public Bitmap decodeBitmap(String filePath, int width, int height) {
        if (width <= 0 || height <= 0) {
            Log.w(TAG, "wrong size");
            return null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(filePath, options);

        int scaleFactor = Math.min(options.outWidth / width, options.outHeight / height);

        options.inJustDecodeBounds = false;
        options.inSampleSize = scaleFactor;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public Bitmap decodeBitmap(int resourceId, int width, int height) {
        if (width <= 0 || height <= 0) {
            Log.w(TAG, "wrong size");
            return null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(context.getResources(), resourceId, options);

        int scaleFactor = Math.min(options.outWidth / width, options.outHeight / height);

        options.inJustDecodeBounds = false;
        options.inSampleSize = scaleFactor;

        return BitmapFactory.decodeResource(context.getResources(), resourceId, options);
    }

    public Bitmap decodeBitmapDisplaySize(String filePath) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return decodeBitmap(filePath, point.x, point.y);
    }

    public Bitmap decodeBitmapDisplaySize(int resourceId) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return decodeBitmap(resourceId, point.x, point.y);
    }

    public BitmapDrawable getScaledResource(int resourceId) {
        Bitmap bitmap = decodeBitmapDisplaySize(resourceId);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public void addImageToGallery(String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(path);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    @SuppressWarnings("RedundantIfStatement")
    public boolean isEqualImage(String path1, String path2) {
        File f1 = new File(path1);
        File f2 = new File(path2);

        if (f1.length() != f2.length()/* && f1.lastModified() != f2.lastModified()*/)
            return false;

        BitmapFactory.Options o1 = new BitmapFactory.Options();
        BitmapFactory.Options o2 = new BitmapFactory.Options();

        o1.inJustDecodeBounds = true;
        o2.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path1, o1);
        BitmapFactory.decodeFile(path2, o2);

        if (o1.outWidth != o2.outWidth || o1.outHeight != o2.outHeight)
            return false;

        return true;
    }
}