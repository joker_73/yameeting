package net.voronoff.yameeting.data.model;

import com.google.gson.annotations.SerializedName;

public class Me {
    @SerializedName("id")
    private int id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("avatar")
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
