package net.voronoff.yameeting.data.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.voronoff.yameeting.data.TokenManager;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.data.model.Me;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface YAMeetingServer {
    String ENDPOINT = "https://yameeting.voronoff.net/";

    @GET("me/")
    Observable<Me> getMe();

    @GET("event/")
    Observable<List<Event>> getEvents();

    @POST("event/")
    Observable<Event> addEvent(@Body Event event);

    @PUT("event/{id}/subscribe/")
    Observable<Event> subscribe(@Path("id") int id);

    @PUT("event/{id}/unsubscribe/")
    Observable<Event> unsubscribe(@Path("id") int id);

    class Creator {
        public static YAMeetingServer create(@NotNull TokenManager tokenManager) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .create();

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new AuthInterceptor(tokenManager))
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(YAMeetingServer.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(httpClient)
                    .build();

            return retrofit.create(YAMeetingServer.class);
        }
    }
}
