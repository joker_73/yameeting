package net.voronoff.yameeting.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.vk.sdk.VKAccessToken;

import net.voronoff.yameeting.data.retrofit.TokenServer;
import net.voronoff.yameeting.data.model.Token;
import net.voronoff.yameeting.injection.ApplicationContext;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.schedulers.Schedulers;

public class TokenManager {
    private static final String TAG = "TokenManager";

    private static final String SHARED_PREFERENCES_NAME = "TokenManager";
    private static final String SP_TOKEN = "Token";

    private SharedPreferences sp;

    private TokenServer tokenServer;
    private Token token;

    public TokenManager(@ApplicationContext Context context) {
        tokenServer = TokenServer.Creator.create();

        sp = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String tokenJson = sp.getString(SP_TOKEN, null);

        if (tokenJson != null) {
            Gson gson = new Gson();
            token = gson.fromJson(tokenJson, Token.class);
        }
    }

    public boolean isAuthorized() {
        return token != null;
    }

    public void setToken(Token token) {
        this.token = token;

        Gson gson = new Gson();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SP_TOKEN, gson.toJson(token));
        editor.apply();
    }

    public String getAccessToken() {
        if (token == null) {
            return null;
        }
        return token.toString();
    }

    public Observable<Token> getToken(@NotNull String username, @NonNull String password) {
        return tokenServer.getToken(username, password)
                .subscribeOn(Schedulers.io())
                .doOnNext(this::setToken);
    }

    public Observable<Token> covertToken(VKAccessToken vkAccessToken) {
        return tokenServer.convertVkToken(vkAccessToken.accessToken, vkAccessToken.email)
                .subscribeOn(Schedulers.io())
                .doOnNext(this::setToken);
    }

    public boolean refreshToken() {
        String refreshToken = token.getRefreshToken();
        if (refreshToken == null) {
            Log.w(TAG, "Refresh token not found");
            return false;
        }

        Call<Token> call = tokenServer.refreshToken(refreshToken);
        Response<Token> response;

        try {
            response = call.execute();
        } catch (IOException e) {
            Log.w(TAG, "Call: " + e.getMessage());
            return false;
        }

        if (!response.isSuccessful()) {
            Log.w(TAG, "Call is failed: " + response.errorBody().toString());
            return false;
        }

        setToken(response.body());
        return true;
    }

    public Observable<Void> revokeToken() {
        return tokenServer.revokeToken(token.getAccessToken())
                .subscribeOn(Schedulers.io())
                .doOnNext(token -> setToken(null));
    }
}
