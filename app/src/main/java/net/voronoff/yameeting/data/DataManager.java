package net.voronoff.yameeting.data;

import android.support.annotation.NonNull;

import net.voronoff.yameeting.data.retrofit.YAMeetingServer;
import net.voronoff.yameeting.data.model.Event;
import net.voronoff.yameeting.data.model.Me;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

public class DataManager {
    @SuppressWarnings("unused")
    private static final String TAG = "DataManager";
    private YAMeetingServer yaMeetingServer;

    public DataManager(TokenManager tokenManager) {
        this.yaMeetingServer = YAMeetingServer.Creator.create(tokenManager);
    }

    public Observable<Me> getMe() {
        return yaMeetingServer.getMe()
                .subscribeOn(Schedulers.io());
    }

    public String getAvatarUrl(Me me) {
        return YAMeetingServer.ENDPOINT + me.getAvatar().substring(1);
    }

    public Observable<List<Event>> getEvents() {
        return yaMeetingServer.getEvents()
                .subscribeOn(Schedulers.io());
    }

    public Observable<Event> addEvent(@NonNull Event event) {
        return yaMeetingServer.addEvent(event)
                .subscribeOn(Schedulers.io());
    }

    public Observable<Event> setSubscribe(int eventId, boolean subscribe) {
        if (subscribe) {
            return yaMeetingServer.subscribe(eventId)
                    .subscribeOn(Schedulers.io());
        } else {
            return yaMeetingServer.unsubscribe(eventId)
                    .subscribeOn(Schedulers.io());
        }
    }
}
