package net.voronoff.yameeting.data.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.voronoff.yameeting.data.model.Token;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface TokenServer {
    String SECRET = "client_id=W7FRLB0wIHs8JGsU1lXPSxm3FckM0ye2OyHJiiev&client_secret=rY7x73zP8PJhWP1s2HARnYi2CRM2LvuPFszR6qsOjCrCRVviqKJobSvrQjHLjTXG9Gh96sduqxJ55QUekMPgJ0Nzjk5FZ6ab5Ayg8CI2nFLQE54ppJaa8jiy2rnFaCr4";

    @POST("/auth/token?grant_type=password&" + SECRET)
    Observable<Token> getToken(@Query("username") String username,
                               @Query("password") String password);

    @POST("auth/convert-token/?grant_type=convert_token&backend=vk-mobile&" + SECRET)
    Observable<Token> convertVkToken(@Query("token") String token, @Query("email") String email);

    @POST("auth/token/?grant_type=refresh_token&" + SECRET)
    Call<Token> refreshToken(@Query("refresh_token") String refreshToken);

    @POST("auth/revoke-token/?" + SECRET)
    Observable<Void> revokeToken(@Query("token") String accessToken);

    class Creator {
        public static TokenServer create() {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .create();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(YAMeetingServer.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

            return builder.build().create(TokenServer.class);
        }
    }
}
