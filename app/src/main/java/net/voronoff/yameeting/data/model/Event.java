package net.voronoff.yameeting.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Event implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String address;

    @SerializedName("created")
    private Date created;

    @SerializedName("where")
    private Date where;

    @SerializedName("author")
    private int author;

    @SerializedName("desc")
    private String desc;

    @SerializedName("subscribed")
    private boolean subscribed;

    @SerializedName("subscribers_number")
    private int subscribersNumber;

    @SerializedName("my")
    private boolean my;

    public Event(String name, String address, Date where, String desc) {
        this.name = name;
        this.address = address;
        this.where = where;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Date getCreated() {
        return created;
    }

    public Date getWhere() {
        return where;
    }

    public int getAuthor() {
        return author;
    }

    public String getDesc() {
        return desc;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public int getSubscribersNumber() {
        return subscribersNumber;
    }

    public boolean isMy() {
        return my;
    }

    protected Event(Parcel in) {
        id = in.readInt();
        name = in.readString();
        address = in.readString();
        author = in.readInt();
        long date = in.readLong();
        where = date == -1 ? null : new Date(date);
        date = in.readLong();
        created = date == -1 ? null : new Date(date);
        desc = in.readString();
        subscribersNumber = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeInt(author);
        dest.writeLong(where != null ? where.getTime() : -1);
        dest.writeLong(created != null ? created.getTime() : -1);
        dest.writeString(desc);
        dest.writeInt(subscribersNumber);
    }

    @Override
    public String toString() {
        return "<Event: \"" + name + "\">";
    }
}
