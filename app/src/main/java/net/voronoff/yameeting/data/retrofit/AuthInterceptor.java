package net.voronoff.yameeting.data.retrofit;

import net.voronoff.yameeting.data.TokenManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    private TokenManager tokenManager;

    public AuthInterceptor(@NotNull TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        Response response;

        String accessToken = tokenManager.getAccessToken();
        if (accessToken != null) {
            Request.Builder requestBuilder = originalRequest.newBuilder()
                    .header("Authorization", accessToken)
                    .method(originalRequest.method(), originalRequest.body());

            response = chain.proceed(requestBuilder.build());
            if (response.code() == 401) {
                synchronized (this) {
                    if (!accessToken.equals(tokenManager.getAccessToken())
                            || tokenManager.refreshToken()) {
                        requestBuilder.header("Authorization", tokenManager.getAccessToken());
                        response = chain.proceed(requestBuilder.build());
                    }
                }
            }
        } else {
            response = chain.proceed(originalRequest);
        }
        return response;
    }
}
