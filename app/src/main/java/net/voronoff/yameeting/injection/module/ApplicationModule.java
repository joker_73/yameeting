package net.voronoff.yameeting.injection.module;

import android.app.Application;
import android.content.Context;

import net.voronoff.yameeting.data.DataManager;
import net.voronoff.yameeting.data.TokenManager;
import net.voronoff.yameeting.data.retrofit.YAMeetingServer;
import net.voronoff.yameeting.injection.ApplicationContext;
import net.voronoff.yameeting.utils.ApplicationUtils;

import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(@NotNull Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    public Context provideContext() {
        return application;
    }

    @Provides
    public ApplicationUtils provideApplicationUtils(@ApplicationContext Context context) {
        return new ApplicationUtils(context);
    }

    @Provides
    @Singleton
    public DataManager provideDataManager(TokenManager tokenManager) {
        return new DataManager(tokenManager);
    }

    @Provides
    @Singleton
    public TokenManager provideTokenManager(@ApplicationContext Context context) {
        return new TokenManager(context);
    }

    @Provides
    public YAMeetingServer provideYAMeetingServer(TokenManager tokenManager) {
        return YAMeetingServer.Creator.create(tokenManager);
    }
}
