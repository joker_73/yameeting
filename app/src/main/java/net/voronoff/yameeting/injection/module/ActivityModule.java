package net.voronoff.yameeting.injection.module;

import android.app.Activity;
import android.content.Context;

import net.voronoff.yameeting.data.DataManager;
import net.voronoff.yameeting.injection.ActivityContext;
import net.voronoff.yameeting.injection.scope.ActivityScope;
import net.voronoff.yameeting.ui.main.MainPresenter;
import net.voronoff.yameeting.utils.BitmapUtils;

import org.jetbrains.annotations.NotNull;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private Activity activity;

    public ActivityModule(@NotNull Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    public Context provideContext() {
        return activity;
    }

    @Provides
    @ActivityScope
    public MainPresenter provideMainPresenter(DataManager dataManager){
        return new MainPresenter(activity, dataManager);
    }

    @Provides
    @ActivityScope
    public BitmapUtils provideBitmapUtils(@ActivityContext Context context) {
        return new BitmapUtils(context);
    }
}
