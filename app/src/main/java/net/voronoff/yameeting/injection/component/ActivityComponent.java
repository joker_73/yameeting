package net.voronoff.yameeting.injection.component;

import net.voronoff.yameeting.injection.module.ActivityModule;
import net.voronoff.yameeting.injection.scope.ActivityScope;
import net.voronoff.yameeting.ui.auth.AuthActivity;
import net.voronoff.yameeting.ui.event.EventActivity;
import net.voronoff.yameeting.ui.main.MainActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {
    FragmentComponent plusFragmentComponent();

    void inject(MainActivity mainActivity);
    void inject(AuthActivity authActivity);
    void inject(EventActivity eventActivity);
}
