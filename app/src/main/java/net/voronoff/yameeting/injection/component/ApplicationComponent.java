package net.voronoff.yameeting.injection.component;

import net.voronoff.yameeting.injection.module.ActivityModule;
import net.voronoff.yameeting.injection.module.ApplicationModule;
import net.voronoff.yameeting.ui.main.fragments.EventsAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    ActivityComponent plusActivityComponent(ActivityModule activityModule);

    void inject(EventsAdapter eventsAdapter);
}
