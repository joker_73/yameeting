package net.voronoff.yameeting.injection.component;

import net.voronoff.yameeting.injection.scope.FragmentScope;
import net.voronoff.yameeting.ui.main.fragments.EventDetailFragment;
import net.voronoff.yameeting.ui.main.fragments.EventListFragment;

import dagger.Subcomponent;

@FragmentScope
@Subcomponent()
public interface FragmentComponent {
    void inject(EventListFragment eventListFragment);
    void inject(EventDetailFragment eventDetailFragment);
}
